import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../shared/data.service';
import { SecurityService } from './security.service';

@Injectable({
  providedIn: 'root'
})
export class AppraisalStatusService {

  constructor(
    private router: Router,
    private securityService: SecurityService,
    private toast: ToastrService,
    private dataService: DataService,
    public toastr: ToastrService
  ) { }

  public AppraisalStatus(IdStatus) {
    let Status = "Pendiente"
    switch (IdStatus) {
      case 0:
        Status = "Rechazada";
        break;
      case 1:
        Status = "Pendiente";
        break;
      case 2:
        Status = "Aceptada";
        break;
      case 3:
        Status = "Visita programada";
        break;
      case 4:
        Status = "Visita completada";
        break;
      default:
        break;
    }
    return Status;
  }
  public VisitStatus2(IdStatus) {
    let Status = "Pendiente"
    switch (IdStatus) {
      case 0:
        Status = "Rechazar";
        break;
      case 1:
        Status = "Pendiente";
        break;
      case 2:
        Status = "Aceptar";
        break;
      default:
        break;
    }
    return Status;
  }
}
