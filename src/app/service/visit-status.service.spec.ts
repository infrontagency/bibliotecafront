import { TestBed } from '@angular/core/testing';

import { VisitStatusService } from './visit-status.service';

describe('VisitStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VisitStatusService = TestBed.get(VisitStatusService);
    expect(service).toBeTruthy();
  });
});
