import { TestBed } from '@angular/core/testing';

import { StorangeLocalService } from './storange-local.service';

describe('StorangeLocalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorangeLocalService = TestBed.get(StorangeLocalService);
    expect(service).toBeTruthy();
  });
});
