import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../shared/data.service';
import { SecurityService } from './security.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(
    private router: Router,
    private securityService: SecurityService,
    private toast: ToastrService,
    private dataService: DataService,
    public toastr: ToastrService
  ) { }

  public InvoiceStatus(IdStatus) {
    // 1 = paid, 2 = cancelled, 3 = Pending paid,  4 = partial payment,5 = Due
    let Status = "Pendiente"
    switch (IdStatus) {

      case 1:
        Status = "Pagada";
        break;
      case 2:
        Status = "Cancelada";
        break;
      case 3:
        Status = "Pendiente de pago";
        break;
      case 4:
        Status = "Pago Parcial";
        break;
      default:
        break;
    }
    return Status;
  }

}
