import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorangeService {
private storange: any;
  constructor() {
    this.storange = sessionStorage;
    //this.storange = localStorage;
   }
   public retrieve(key: string): any{
     const item = this.storange.getItem(key);
     if(item && item !== 'undefined'){
       return JSON.parse(item);
     }
     return;
   }
   public store(key: string , value: any){
      this.storange.setItem(key, JSON.stringify(value));
   }
}
