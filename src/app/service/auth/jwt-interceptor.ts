import { ToastrService } from 'ngx-toastr';
import { SecurityService } from './../security.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private securityService: SecurityService,
    private toast: ToastrService
  ) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.securityService.GetToken();
    console.log('intercep', token);
    if (token) {
      // clone: https://angular.io/guide/http#immutability
      req = req.clone({
        setHeaders: {
          // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(req);
  }
}
