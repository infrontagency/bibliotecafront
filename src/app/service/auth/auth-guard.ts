import { ToastrService } from 'ngx-toastr';
import { SecurityService } from './../security.service';
import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, CanActivate } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/shared.service';
import { Iresponse } from 'src/app/models/iresponse';
import { environment } from "../../../environments/environment.prod"

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  subRef$: Subscription;
  readonly rootURL = environment.baseUrl;
  dataUser: any;
  constructor(
    private router: Router,
    private securityService: SecurityService,
    private toast: ToastrService,
    private dataService: DataService,
    public toastr: ToastrService

  ) { }
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;
  public GetCurrentUser() {
    this.subRef$ = this.dataService.get<Iresponse>(this.rootURL + 'auth/GetCurrentUser')
      .subscribe(res => {
        this.dataUser = res.body;
      }, err => {
        this.toastr.error('No fue posible iniciar tu cuenta', 'Registro');
        this.router.navigate(['./']);

      });
  }

  canActivate() {
    // const data = this.userController.GetCurrentUser();
    //if(data){console.log(data); return false}
    this.GetCurrentUser();

    if (this.securityService.IsAuthorized) {
      console.log("authorize");
      return true;
    }


    this.router.navigate(['./']);

    return false;
  }
}
