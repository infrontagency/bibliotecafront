import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { UpdateBookComponent } from './admin/update-book/update-book.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './service/auth/auth-guard';

import { AboutusComponent } from './user/aboutus/aboutus.component';
import { DetailsBookComponent } from './user/details-book/details-book.component';
import { ListBookComponent } from './user/list-book/list-book.component';
import { RootComponent } from './user/root/root.component';
import { UserComponent } from './user/user.component';
import { UserModule } from './user/user.module';
//const routes: Routes = [];
const routes: Routes = [
  
  {
    path: '', component: UserComponent,
    children: [{
      path: '',
      component: RootComponent
    },
    {
      path: 'aboutus',
      component: AboutusComponent
    },
    {
      path: 'listbook',
      component: ListBookComponent
    },
    {
      path: 'detailsBook/:idBooks',
      component: DetailsBookComponent
    }

    ]
  },
  
  {
    path:'',
    component:AdminComponent,
    children:[{
      path:'dashboard',
      component:DashboardComponent,
      canActivate: [AuthGuard]
    },
    {
      path:'updateBook/:idBooks',
      component:UpdateBookComponent,
      canActivate: [AuthGuard]
    }]
  },
  {
    path:'',
    component:AuthComponent,
    children:[{
      path:'login',
      component:LoginComponent
    },
    {
      path:'register',
      component:RegisterComponent
    },]
  },

   { path: 'login', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
   { path: 'register', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },/**/
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
