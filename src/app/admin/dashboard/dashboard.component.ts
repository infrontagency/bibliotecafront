import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Iresponse } from 'src/app/models/iresponse';
import { DataService } from 'src/app/shared/shared.service';
import { environment } from 'src/environments/environment.prod';
import * as moment from 'moment';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { NavigationEnd, Router } from '@angular/router';
import { SecurityService } from 'src/app/service/security.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  IsAuthenticated = false;
  private subsAuth;

  subRef: any;
  readonly api = environment.baseUrl;
  result: any;
  Books: any;
  editorials: any;
  bookGenres: any;
  profileForm = new FormGroup({
    Name: new FormControl('', Validators.required),
    synapse: new FormControl('', Validators.required),
    NumberPages: new FormControl('', Validators.required),
    PublicationDate: new FormControl('', Validators.required),
    IdBookGenre: new FormControl('', Validators.required),
    IdEditorials: new FormControl('', Validators.required),
  });
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private toastr: ToastrService,
    private router: Router,
    private securityService: SecurityService
  ) {

    this.IsAuthenticated = this.securityService.IsAuthorized;

    //Observable event auth
    this.subsAuth = this.securityService.authChanllenge$.subscribe((isAuth) => {
      this.IsAuthenticated = isAuth;

      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      }

      this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
          // trick the Router into believing it's last link wasn't previously loaded
          this.router.navigated = false;
          // if you need to scroll back to top, here is the right place
          window.scrollTo(0, 0);
        }
      });


    });
  }
  ngOnInit(): void {
    this.GetBooks();
    this.GetEditorials();
    this.GetBookGenres();
  }
  GetBooks() {
    this.subRef = this.dataService.get<Iresponse>(this.api + 'books/')
      .subscribe(res => {
        this.Books = res.body;
        console.log(this.Books);
      }, err => {
        this.toastr.error('No fue posible cargar los libros', 'Libros');

      });
  }
  GetEditorials() {
    this.subRef = this.dataService.get<Iresponse>(this.api + 'Editorial/')
      .subscribe(res => {
        this.editorials = res.body;
        console.log(this.editorials);
      }, err => {
        this.toastr.error('No fue posible cargar los libros', 'Libros');

      });
  }
  GetBookGenres() {
    this.subRef = this.dataService.get<Iresponse>(this.api + 'bookgenres/')
      .subscribe(res => {
        this.bookGenres = res.body;
        console.log(this.bookGenres);
      }, err => {
        this.toastr.error('No fue posible cargar los libros', 'Libros');

      });
  }
  onSubmit() {

    this.CreateBook(this.profileForm.value);
  }
  CreateBook(data: any) {
    console.log(data);

    var momentVariable = moment(data.PublicationDate, 'MM-DD-YYYY');
    var from = momentVariable.format('YYYY-MM-DD');
    data.PublicationDate = "2019-01-06T17:16:40";
    data.NumberPages = +data.NumberPages;
    data.IdBookGenre = +data.IdBookGenre;
    data.IdEditorials = +data.IdEditorials;
    this.subRef = this.dataService.post<Iresponse>(this.api + 'books/createbook', data)
      .subscribe(res => {
        this.result = res.body;
        console.log(this.result);
        this.toastr.info('Libro creado', 'Libros');
        const token = this.result.response;
        this.router.navigate(['dashboard']);
        Swal.fire({
          icon: 'success',
          title: 'Se ha registrado un nuevo libro',
          showConfirmButton: false,
          timer: 1500
        })
        this.GetBooks();
      }, err => {
        // this.toastr.error('No fue posible cargar los paises', 'Registro');
        console.log(err);
      });
  }
  deleteBook(idBooks: any) {
    Swal.fire({
      title: '¿Quieres eliminar este libro?',
      showCancelButton: true,
      denyButtonText: `Don't save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {

        this.subRef = this.dataService.delete<Iresponse>(this.api + 'books/' + idBooks).subscribe(res => {
          Swal.fire('Saved!', '', 'success')
          this.GetBooks();
        }, err => {
          Swal.fire('No fue posible eliminar el registro!', '', 'error')

        });
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    });

  }
  closed() {
    this.securityService.LogOff();

  }

}

