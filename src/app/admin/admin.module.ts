import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FooterAdminComponent } from './footer-admin/footer-admin.component';
import { HeaderAdminComponent } from './header-admin/header-admin.component';
import { BrowserModule } from '@angular/platform-browser';
import { UpdateBookComponent } from './update-book/update-book.component';



@NgModule({
  declarations: [
  
    AdminComponent,
    
    
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    BrowserModule
  ]
})
export class AdminModule { }
