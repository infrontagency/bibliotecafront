export interface Payments {
  IdBill:number;
  IdPayment:number;
  Description:string;
  Value:string;
  PaymentDate:Date;
  File:string;
  IdAuthor:number;
}
