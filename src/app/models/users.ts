
import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
export interface Users {
  User: string;
  IdProfile: number;
  Password: string;
  confirmPassword: string;
  Email: string;
  IdTypeDocument: number;
  DocumentNumber: string;
  Status: number;
  IdRol: number;
  Name: string;
  IdCountry: number;
  IdStates: number;
  IdCity: number;
  Lat: number;
  Lng: number;
  Mobile: string;
  Address: string;
  CreationDate: Date;
  DesciptionAddress: string;
  IdAppraiser: number;
  IdCustomer: number;
  IdBank: number;
  IdLawyer: number;
  IdCompany:number;
  IdRealState: number;
  SocialNetworks:string;
}
export const validarQueSeanIguales: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const password = control.get('Password');
  const confirmarPassword = control.get('confirmPassword');

  return password.value === confirmarPassword.value ? null : { 'noSonIguales': true };
};
