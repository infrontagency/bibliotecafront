export interface VisitingCalendar {
  IdVisitingCalendar: number;
  SchedulingDay: Date;
  IdAuthor: number;
  IdAppraisal: number;
  VisitingDay: Date;
  FinishVisitingDay: Date;
  IdUserGuest: number;
  IdStatus: number; // StatusAppraisal
  Description: string;
  Name: string;
  Status:number;
}
