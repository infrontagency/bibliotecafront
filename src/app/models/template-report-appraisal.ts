export interface TemplateReportAppraisal {
  IdTemplateReportAppraisal: number;
  Name: string;
  IdAppraiser: number;
}
