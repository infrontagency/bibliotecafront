export interface BanchCompany {
  CreationDate: Date;
  Name: string;
  Address: string;
  DesciptionAddress: string;
  ProfileImage: string;
  Ans: number;
  Email: string;
  DocumentNumber: string;
  Mobile: string;
  SocialNetworks: string;
  Lat: number;
  Lng: number;
  IdTypeDocument: number;
  IdCountry: number;
  IdStates: number;
  IdCity: number;
  Status: number;
  IdProfile: number;
  IdCustomersType: number;
  IdCompany:number;
}
