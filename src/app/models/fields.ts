export interface Fields {
  IdField:number;
  Name:string;
  IdTypeField:number;
  Status:number;
  Content:string;
  Config:string;
  Required:number;
  Weight:number;
  IdCategory:number;
}
