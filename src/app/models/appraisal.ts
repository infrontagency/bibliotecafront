export interface Appraisal {
  Cod: string;
  CodEntidad: string;
  IdStates: number;
  IdAuthor: number;
  IdCountry: number;
  IdCity: number;
  IdStatus: number;
  IdCustomers: number;
  IdAppraiser: number;
  IdProduct: number;
  Lat: number;
  Lng: number;
  Address:string;
  DescriptionAddress:string;
  RegisterDate:Date;
  IdBranchCompanies:number;
  RegistryNumber:string;
  IdCurrentUsage:number;
  IdPropertyType:number;
  AppraisalCost:string;
  CollectedValue:string;
  DateofAssignment:Date;
  IdPriority:number;
  IdSector:number;
  IdTypeDocument:number;
  OwnerName:string;
  OwnerNumberId:string;
  OwnerEmail:string;
  OwnerPhone:string;

  ContactName:string;
  ContactNumberId:string;
  ContactEmail:string;
  ContactPhone:string;
}
