export interface Appraisers {
  CreationDate: Date;
  Name: string;
  Address: string;
  DesciptionAddress: string;
  Email: string;
  DocumentNumber: string;
  Mobile: string;
  SocialNetworks: string;
  Lat: number;
  Lng: number;
  IdTypeDocument: number;
  IdCountry: number;
  IdStates: number;
  IdCity: number;
  Status: number;
  IdAppraiser: number;
}
