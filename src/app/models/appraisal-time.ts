import { Data } from '@angular/router';

export interface AppraisalTime {
  IdAppraisalTimes: number;
  IdAuthor: number;
  IdAppraisals: number;
  StartEvent: Date;
  FinishEvent: Date;
  IdUserGuest: 0;
  IdStatus: number;//StatusAppraisal
  Descripcion: string;
}
