export interface BinnacleComments {
  IdAuthor:number;
  Name:string;
  published:Date;
  IdAppraisal:number;
  Comment:string;
}
