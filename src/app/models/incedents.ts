export interface Incedents {
  IdIncidents:number;
  Description:string;
  CreationDate:Date;
  IdAppraisal:number;
  IdAuthor:number;
  IdCategoryIncidents:number;
  Conversation:string;
  Status:number; // 1 - Open, 2 - Closed, 3 - resolved, 4 unfair
}
