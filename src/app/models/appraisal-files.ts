export interface AppraisalFiles {
  IdAppraisalFile:number;
  NameAppraisalFile:string;
  DateUpdate:Date;
  IdAuthor:number;
  IdCategoyFiles:number;
  IdApprisal:number;
  Description:string;
  FileName:string;
}
