import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Iresponse } from 'src/app/models/iresponse';
import { SecurityService } from 'src/app/service/security.service';
import { DataService } from 'src/app/shared/shared.service';
import { environment } from 'src/environments/environment.prod';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],

})
export class LoginComponent implements OnInit {
  readonly api = environment.baseUrl;
  subRef: any;
  result: any;

  profileForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private fb: FormBuilder,
    private dataService: DataService,
    private toastr: ToastrService,
    private securityService: SecurityService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    /*this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email], 
              this.checkValidEmail]
    })  */

  }
  checkValidEmail(control: AbstractControl) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'super@secret.com') {
          resolve({ emailIsTaken: true })
        } else { resolve(null) }
      }, 2000)
    })
  }
  onSubmit() {
    console.log(this.profileForm.value);
    this.login(this.profileForm.value);
  }
  login(data: any) {
    console.log('metodo');
    this.subRef = this.dataService.post<Iresponse>(this.api + 'auth/' + 'login', data)
      .subscribe(res => {
        this.result = res.body;
        console.log(this.result);
        this.toastr.info('Se ha realizado el Login', 'Login');
        const token = this.result.response;
        this.securityService.SetAuthData(token);
        this.router.navigate(['dashboard']);
        this.toastr.success('No fue posible cargar los paises', 'Registro');
        Swal.fire('Hello world!')

      }, err => {
        // this.toastr.error('No fue posible cargar los paises', 'Registro');
        console.log(err);
      });
  }
}
