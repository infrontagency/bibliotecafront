import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  form: FormGroup ;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email], 
              this.checkValidEmail]
    })  }
    checkValidEmail(control: AbstractControl) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (control.value === 'super@secret.com') {
              resolve({ emailIsTaken: true })
          } else {resolve(null)}
        }, 2000)
      })
  }
}
