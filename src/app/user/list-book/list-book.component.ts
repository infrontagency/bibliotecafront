import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Iresponse } from 'src/app/models/iresponse';
import { DataService } from 'src/app/shared/shared.service';
import { environment } from 'src/environments/environment.prod';
import { BooksService } from 'src/app/service/books.service';
@Component({
  selector: 'app-list-book',
  templateUrl: './list-book.component.html',
  styleUrls: ['./list-book.component.sass']
})
export class ListBookComponent implements OnInit {

  subRef: any;
  readonly api = environment.baseUrl;
  result: any;
  Books: any;
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private toastr: ToastrService,
    private bookService: BooksService
  ) { }
  ngOnInit(): void {
    this.GetBooks();
  }
  /**/GetBooks() {
    this.subRef = this.dataService.get<Iresponse>(this.api + 'books/')
      .subscribe(res => {
        this.Books = res.body;
        console.log(this.Books);
      }, err => {
        this.toastr.error('No fue posible cargar los libros', 'Libros');

      });
  }
}
