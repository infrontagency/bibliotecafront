import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RootComponent } from './root/root.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { UserComponent } from './user.component';
import { ListBookComponent } from './list-book/list-book.component';
import { LoginComponent } from '../auth/login/login.component';
import { DetailsBookComponent } from './details-book/details-book.component';
const routes: Routes = [{
  path: "",
  component: UserComponent,
  loadChildren: () => import('./root/root.component').then(m => m.RootComponent),
  children: [
    { path: '', redirectTo: 'RootComponent', pathMatch: 'prefix' },
    { path: 'aboutus', loadChildren: () => import('./aboutus/aboutus.component').then(m => m.AboutusComponent) },
    { path: 'listbook', component: ListBookComponent },
    { path: 'login', component: LoginComponent },
    {
      path: 'detailsBook:idBooks',
      component: DetailsBookComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
