import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/shared.service';
import { Iresponse } from 'src/app/models/iresponse';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.sass']
})
export class AboutusComponent implements OnInit {
  subRef: any;
  readonly root = 'http://localhost:90/WeatherForecast';
  result: any;
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.weather();
    //this.metodo2();

  }
  metodo2() {
    this.http.get(this.root)
      .subscribe(data => {
        console.log(data);
      });
  }
  weather() {
    console.log('metodo');
    this.subRef = this.dataService.get<Iresponse>(this.root)
      .subscribe(res => {
        this.result = res.body;
        console.log(this.result);
        this.toastr.success('No fue posible cargar los paises', 'Registro');
        Swal.fire('Hello world!')

      }, err => {
        // this.toastr.error('No fue posible cargar los paises', 'Registro');
        console.log(err);
      });
  }

}
