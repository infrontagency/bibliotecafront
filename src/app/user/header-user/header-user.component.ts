import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SecurityService } from 'src/app/service/security.service';
import { DataService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-header-user',
  templateUrl: './header-user.component.html',
  styleUrls: ['./header-user.component.sass']
})
export class HeaderUserComponent implements OnInit {
  IsAuthenticated = false;
  private subsAuth;
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private toastr: ToastrService,
    private router: Router,
    private securityService: SecurityService
  ) { 
    this.IsAuthenticated = this.securityService.IsAuthorized;

    //Observable event auth
    this.subsAuth = this.securityService.authChanllenge$.subscribe((isAuth) => {
      this.IsAuthenticated = isAuth;
      
      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      }

      this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
          // trick the Router into believing it's last link wasn't previously loaded
          this.router.navigated = false;
          // if you need to scroll back to top, here is the right place
          window.scrollTo(0, 0);
        }
      });


    });
  }

  ngOnInit(): void {
  }
  closed(){
    this.securityService.LogOff();
    
  }
}
