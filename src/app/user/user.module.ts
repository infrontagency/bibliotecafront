import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { RootComponent } from './root/root.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { HeaderUserComponent } from './header-user/header-user.component';
import { FooterUserComponent } from './footer-user/footer-user.component';
import { UserComponent } from './user.component';
import { ListBookComponent } from './list-book/list-book.component';
import { HttpClientModule } from '@angular/common/http';
import { DetailsBookComponent } from './details-book/details-book.component';


@NgModule({
  declarations: [
    RootComponent,
    AboutusComponent,
    HeaderUserComponent,
    FooterUserComponent,
    UserComponent,
    ListBookComponent,
    DetailsBookComponent

  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    HttpClientModule
  ]
})
export class UserModule { }
