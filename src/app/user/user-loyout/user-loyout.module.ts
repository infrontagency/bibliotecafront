import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserLoyoutRoutingModule } from './user-loyout-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UserLoyoutRoutingModule
  ]
})
export class UserLoyoutModule { }
