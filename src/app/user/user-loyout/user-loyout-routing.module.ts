import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutusComponent } from '../aboutus/aboutus.component';
import { ListBookComponent } from '../list-book/list-book.component';
import { RootComponent } from '../root/root.component';

const routes: Routes = [
  {
    path:'',
    component:RootComponent
  },
  {
    path:'aboutus',
    component:AboutusComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserLoyoutRoutingModule { }
