import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Iresponse } from 'src/app/models/iresponse';
import { BooksService } from 'src/app/service/books.service';
import { DataService } from 'src/app/shared/shared.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.sass']
})
export class RootComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private toastr: ToastrService,
    private bookService: BooksService
  ) { }
  readonly api = environment.baseUrl;

  Books: any;
  subRef:any;
  ngOnInit(): void {
    this.GetBooks();
  }
  GetBooks() {
    this.subRef = this.dataService.get<Iresponse>(this.api + 'books/')
      .subscribe(res => {
        this.Books = res.body;
        console.log(this.Books);
      }, err => {
        this.toastr.error('No fue posible cargar los libros', 'Libros');

      });
  }

}
